
for ii in `seq 0.1 0.1 3.0`; 
do 
    SUFIX=`echo ${ii} | awk -F. '{print $1$2}'` 
    cat proc_card_ZBFHH.dat | sed -e s/sm/sm${SUFIX}/g | sed -e s/ZBFHH/ZBFHHL${SUFIX}/g > proc_card_ZBFHHL${SUFIX}.dat 
done

for ii in `seq 0.1 0.1 3.0`; 
do 
    SUFIX=m`echo ${ii} | awk -F. '{print $1$2}'` 
    cat proc_card_ZBFHH.dat | sed -e s/sm/sm${SUFIX}/g | sed -e s/ZBFHH/ZBFHHL${SUFIX}/g > proc_card_ZBFHHL${SUFIX}.dat 
done
