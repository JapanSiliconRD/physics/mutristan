


for  ii in /home/kojin/work/Physics/LGADPhys/evnt_100TeV/log.mc15* 
do
    NAME=`basename ${ii}`
    NAME=`echo $NAME | awk -F. '{print $4}'`
    PROCNAME=proc_card_${NAME#MGPy8EG_}.dat
    cat ${ii}/proc_card_mg5.dat | sed -e s/"output \-f"/"output ppcollider_${NAME#MGPy8EG_}_v1"/g | sed -e s/"mssm\-full"/"MSSM\_SLHA2"/g > $PROCNAME
done
