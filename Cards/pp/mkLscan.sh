
for ii in `seq 0.1 0.1 3.0`; 
do 
    SUFIX=`echo ${ii} | awk -F. '{print $1$2}'` 
    cat proc_card_ggFHH.dat | sed -e s/loop\_sm/loop\_sm${SUFIX}/g | sed -e s/ggFHH/ggFHHL${SUFIX}/g > proc_card_ggFHHL${SUFIX}.dat 
done

for ii in `seq 0.1 0.1 3.0`; 
do 
    SUFIX=m`echo ${ii} | awk -F. '{print $1$2}'` 
    cat proc_card_ggFHH.dat | sed -e s/loop\_sm/loop\_sm${SUFIX}/g | sed -e s/ggFHH/ggFHHL${SUFIX}/g > proc_card_ggFHHL${SUFIX}.dat 
done
