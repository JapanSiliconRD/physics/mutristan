#!/bin/bash

BEAM1=30   
BEAM2=1000
SUFIX=""
COLTYPE=""
#NUMEVENT=500000
NUMEVENT=10000

if [ `which python3.8 | wc -l ` -eq 0 ]
then
    PYTHONCOM=python3.7
else
    PYTHONCOM=python3.8
fi
    

if [ ! $1 ]
then
    echo "$0 [proc card] (muon energy)"
    exit
fi
if [ $2 ]
then
    BEAM1=$2
fi
if [ $3 ]
then
    BEAM2=$3
fi

BEAMSETTING=${BEAM1}GeV${BEAM2}GeV

if [ $4 ]
then
    SUFIX=$4
    BEAMSETTING=${BEAMSETTING}_${SUFIX}
fi


PROCNAME=`basename $1`
COLTYPE=`dirname $1`
COLTYPE=`basename $COLTYPE`
OUTDIR=`grep "^output" $1 | awk '{print $2}'`
NEWOUTDIR=$OUTDIR
cat $1 | sed s/"${OUTDIR}"/"${OUTDIR%_v1}_${BEAMSETTING}_v1"/g > $1_${BEAMSETTING}_tmp
NEWOUTDIR=`echo ${OUTDIR} | sed s/"${OUTDIR}"/"${OUTDIR%_v1}_${BEAMSETTING}_v1"/g`

$PYTHONCOM ./bin/mg5_aMC $1_${BEAMSETTING}_tmp
rm -f  $1_${BEAMSETTING}_tmp

if [ $COLTYPE == "pp" ]
then
    cat $NEWOUTDIR/Cards/run_card_default.dat | sed s/"10000 = nevents"/"${NUMEVENT} = nevents"/g | sed s/"6500.0     = ebeam1"/"${BEAM1}     = ebeam1"/g | sed s/"6500.0     = ebeam2"/"${BEAM2}   = ebeam2"/g | sed s/"0.0     = polbeam1"/"-70.0     = polbeam1"/g | sed s/"0.0     = polbeam2"/"+80.0     = polbeam2"/g | sed s/"10.0  = ptl "/" 0.0  = ptl "/g | sed s/"2.5  = etal"/"5.5  = etal"/g  > $NEWOUTDIR/Cards/run_card.dat
elif  [ $COLTYPE == "emu" -o $COLTYPE == "mumu" ]
then
    cat $NEWOUTDIR/Cards/run_card_default.dat | sed s/"10000 = nevents"/"${NUMEVENT} = nevents"/g | sed s/"500.0     = ebeam1"/"${BEAM1}     = ebeam1"/g | sed s/"500.0     = ebeam2"/"${BEAM2}   = ebeam2"/g | sed s/"0.0     = polbeam1"/"-70.0     = polbeam1"/g | sed s/"0.0     = polbeam2"/"+80.0     = polbeam2"/g | sed s/"10.0  = ptl "/" 0.0  = ptl "/g | sed s/"2.5  = etal"/"5.5  = etal"/g  > $NEWOUTDIR/Cards/run_card.dat
elif  [ $COLTYPE == "iwwamu" ]
then
    cat $NEWOUTDIR/Cards/run_card_default.dat | sed s/"10000 = nevents"/"${NUMEVENT} = nevents"/g \
	| sed s/"1        = lpp1"/"-4        = lpp1"/g \
	| sed s/"nn23lo1    = pdlabel1"/"iww    = pdlabel1"/g \
	| sed s/"nn23lo1    = pdlabel2"/"none   = pdlabel2"/g \
	| sed s/"6500.0     = ebeam1"/"${BEAM1}     = ebeam1"/g \
	| sed s/"1000.0     = ebeam2"/"${BEAM2}   = ebeam2"/g \
	| sed s/"0.0     = polbeam1"/"0.0     = polbeam1"/g \
	| sed s/"0.0     = polbeam2"/"0.0     = polbeam2"/g \
	| sed s/"10.0  = ptl "/" 0.0  = ptl "/g \
	| sed s/"2.5  = etal"/"5.5  = etal"/g  > $NEWOUTDIR/Cards/run_card.dat
else
    echo "unknown type : " $COLTYPE
    exit
fi
cd $NEWOUTDIR/Cards
cp pythia8_card_default.dat pythia8_card.dat
$PYTHONCOM ../bin/generate_events 




