#!/bin/bash

if [ ! $1 ]
then
    echo "usage : $0 [sample name]"
    exit
fi
INPUTFILE=../mg5/MG5_aMC_v3_4_0/$1/Events/run_01/tag_1_pythia8_events.hepmc.gz
mkdir -p ../delphout
OUTPUTFILE=../delphout/$1.root
if [ ! -f $INPUTFILE ]
then
    echo "file : $INPUTFILE not found"
    exit
fi

if [ -f $OUTPUTFILE ]
then
    echo "file : $OUTPUTFILE exist"
    rm  $OUTPUTFILE
fi
echo "gunzip -c $INPUTFILE | ./DelphesHepMC2 cards/delphes_card_HLLHC.tcl $OUTPUTFILE"
gunzip -c $INPUTFILE | ./DelphesHepMC2 cards/delphes_card_HLLHC.tcl $OUTPUTFILE


