#!/bin/bash

CHANNEL=VBFtopm
if [ ! $1 ]
then
    echo "usage : $0 [process] (muon energy)"
    exit
else
    if [ ! -f mg5/MG5_aMC_v3_4_0/Cards/proc_card_$CHANNEL.dat ]
    then
	echo "Cards/proc_card_$CHANNEL.dat : no such file"
	exit
    fi
fi
CHANNEL=$1

cd mg5/MG5_aMC_v3_4_0 
./runmg5.sh Cards/proc_card_${CHANNEL}.dat $2
cd ../../


cd Delphes-3.5.0/
./rundelphes.sh emucollider_${CHANNEL}$2_v1 

