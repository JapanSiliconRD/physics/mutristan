# muTristan



## Introduction

This repositry is just including instruction how to run muTristan software.


## installation of packages

Need to install following before install software :
- ROOT (v6.24.06 or later)
- python (v3.7 or later)

at muTristan1.kek.jp
```
source /usr/local/ROOT/v6.24.06/bin/thisroot.sh
```

at Cern CentOS7 (e.g atlaspc7.kek.jp)
```
source /opt/rh/devtoolset-7/enable
```

### checkout this package

```
cd [work dir you want to install]
git clone http://gitlab.cern.ch/JapanSiliconRD/Physics/muTristan.git
```

### installing madgraph

```
mkdir mg5
cd mg5
wget https://launchpad.net/mg5amcnlo/3.0/3.4.x/+download/MG5_aMC_v3.4.0.tar.gz
tar -zxvf MG5_aMC_v3.4.0.tar.gz
cd MG5_aMC_v3_4_0/
python3.8 bin/mg5_aMC  or  python3.7 bin/mg5_aMC
	  install pythia8
	  install MadAnalysis5
	  exit
mv input/mg5_configuration.txt input/mg5_configuration.txt_orig
cat input/mg5_configuration.txt_orig | sed s/"automatic_html_opening = True"/"automatic_html_opening = False"/g > input/mg5_configuration.txt

ln -s ../../muTristan/runmg5.sh . 
ln -s ../../muTristan/Cards . 
chmod 755 runmg5.sh
cd ../../
```

### installing Delphes

```
wget http://cp3.irmp.ucl.ac.be/downloads/Delphes-3.5.0.tar.gz
tar -zxvf Delphes-3.5.0.tar.gz
cd Delphes-3.5.0
make
ln -s ../muTristan/rundelphes.sh . 
cd ../
mkdir delphout
```

### installing analysis package of Delphes output

```
git clone https://gitlab.cern.ch/JapanSiliconRD/physics/DelphesAnalysisProject.git
cd DelphesAnalysisProject/config
./configure.sh
source setup_local.sh
mkdir builds
cd builds
cmake3 ..
make
make install
cd ../../
```


## Production of MC data samples

### madgraph with pythia8

```
cd [work dir you want to install]
cd mg5/MG5_aMC_v3_4_0
./runmg5.sh Cards/emu/proc_card_ZBFHbb.dat
```

This command generate mg5/MG5_aMC_v3_4_0/emucollider_ZBFHbb_v1 directory.
Check if Event file is generated e.g.  Events/run_01/tag_1_pythia8_events.hepmc.gz

Default beam energy set to 30GeV electron and 1000GeV mu+.
If you add second (Ee) and third (Emu) arguments, beam energy can be modified.

```
./runmg5.sh Cards/emu/proc_card_ZBFHbb.dat 60 500
```
This command generate mg5/MG5_aMC_v3_4_0/emucollider_ZBFHbb60GeV500GeV_v1 directory.


### run Delphes

```
cd [work dir you want to install]
cd Delphes-3.5.0
./rundelphes.sh emucollider_ZBFHbb_v1
```

This command generate rootfile in delphout dir in work dir.
Checl if rootfile generated e.g. delphout/emucollider_WBFHbb_v1.root

### analysis

```
cd [work dir you want to install]
cd DelphesAnalysisProject
## modify runfiles/* to point to generated root file
./bin/DelphesAnalysis < runfiles/emucollider_ZBFHbb_v1_ZBFHbbAnalysis 
```
